import 'react-native-gesture-handler';
import React from 'react';
import {Provider} from 'react-redux';
import {Store, Persistor} from './src/Store/store';

import Router from './src/Router/router';

export default function App() {
  return (
    <Provider store={Store}>
      <Router />
    </Provider>
  );
}
