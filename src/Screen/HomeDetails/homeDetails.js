import React from 'react';
import {
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

import {Avatar, Button} from 'react-native-elements';

import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import FastImage from 'react-native-fast-image';
import logo from '../../Assets/Img/appla-logo.png';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Foundation from 'react-native-vector-icons/Foundation';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';

import Swiper from '../../Assets/Component/Swiper';
import {styles} from './styles';

export default function HomeDetails(props) {
  // const goBack = () => {
  //   props.navigation.goBack();
  // };

  return (
    <SafeAreaView style={styles.SafeAreaView}>
      <View style={styles.appbar}>
        <TouchableOpacity //onPress={goBack}
        >
          <Ionicons name="md-chevron-back-outline" size={32} color="grey" />
        </TouchableOpacity>
        <Text>Title</Text>
        <TouchableOpacity>
          <FontAwesome name="shopping-cart" color="grey" size={32} />
        </TouchableOpacity>
      </View>
      <ScrollView style={styles.ScrollView}>
        <View style={styles.swiper}>
          <Swiper />
        </View>
        <View style={styles.price}>
          <Text>Price</Text>
          <TouchableOpacity>
            <FontAwesome name="heart-o" color="grey" size={32} />
          </TouchableOpacity>
        </View>
        <View style={{paddingHorizontal: widthPercentageToDP(6)}}>
          <Text>Title</Text>
        </View>
        <View style={styles.title}>
          <Foundation name="star" color="grey" size={16} />
          <Text style={styles.paddingleft10}>Rating | Sold 0</Text>
        </View>
        <TouchableOpacity style={styles.compare}>
          <Text style={styles.textCompare}>COMPARE</Text>
        </TouchableOpacity>
        <View style={styles.Avatar}>
          <Avatar
            rounded
            source={{
              uri: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
            }}
            size="medium"
          />
          <View
            style={{
              marginLeft: widthPercentageToDP(2),
            }}>
            <Text>Shop</Text>
            <View style={styles.flexrow}>
              <Entypo name="location-pin" size={16} color="grey" />
              <Text style={{paddingLeft: widthPercentageToDP(1)}}>
                Location
              </Text>
            </View>
            <View style={styles.flexrow}>
              <Entypo name="star" size={16} color="yellow" />
              <Text style={styles.paddingleft1}>Ratings</Text>
            </View>
            <View style={{marginTop: heightPercentageToDP(1)}}>
              <Button title="  + FOLLOW  " type="outline" />
            </View>
          </View>
        </View>
        <View style={styles.description}>
          <Text style={styles}>Description</Text>
        </View>
        <View style={styles.condition}>
          <Text>Condition</Text>
          <Text style={{marginLeft: widthPercentageToDP(25)}}>New</Text>
        </View>
        <View style={styles.minOrder}>
          <Text>Min Order</Text>
          <Text style={{marginLeft: widthPercentageToDP(25)}}>1</Text>
        </View>
        <View style={styles.minOrder}>
          <Text>Weight</Text>
          <Text style={{marginLeft: widthPercentageToDP(28)}}>500 Gr</Text>
        </View>
        <View style={styles.line} />
        <View
          style={{
            marginHorizontal: widthPercentageToDP(6),
            marginTop: heightPercentageToDP(2),
          }}>
          <Text style={styles.textSpesification}>Spesification</Text>
        </View>
        <View
          style={{
            marginHorizontal: widthPercentageToDP(6),
            marginTop: heightPercentageToDP(2),
          }}>
          <Text style={{textAlign: 'justify'}}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras id
            interdum justo. Quisque pretium nulla vitae pulvinar elementum. Sed
            urna tortor, pretium eget lacinia vitae, lacinia ac massa. Nulla at
            odio lacus. Morbi ac pretium dui. Fusce sed enim pretium, viverra
            tortor at, laoreet augue. Ut sed pulvinar dolor. Mauris et mauris
            mauris. Pellentesque ultricies mauris vel est tempus sagittis. Sed
            scelerisque ante magna, sit amet hendrerit purus facilisis vel.
            Aenean quis enim scelerisque, mollis ligula sit amet, faucibus est.
            Nam in tellus cursus, lobortis velit nec, mattis quam. Mauris
            lobortis leo a dapibus faucibus. Praesent ut rhoncus odio.
          </Text>
        </View>
        <View
          style={{
            marginHorizontal: widthPercentageToDP(6),
          }}>
          <Text style={styles.textReadMore}>Read More</Text>
        </View>
        <View style={styles.allReview}>
          <Text style={styles.textAllReview}>All Review</Text>
        </View>
        <View style={styles.image}>
          <FastImage source={logo} style={styles.bgImg} resizeMode="stretch" />
          <Text style={styles.noReview}>This Product Have No Review Yet</Text>
        </View>
      </ScrollView>
      <View style={styles.footer}>
        <Button
          title="Buy Now"
          type="outline"
          containerStyle={{width: widthPercentageToDP(30)}}
        />
        <Button
          title="Add To Cart"
          type="outline"
          buttonStyle={styles.buttonAddToCart}
          titleStyle={styles.textAddToCart}
        />
      </View>
    </SafeAreaView>
  );
}
