import {StyleSheet} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

export const styles = StyleSheet.create({
  bgImg: {
    height: heightPercentageToDP(5),
    width: widthPercentageToDP(25),
  },
  allReview: {
    marginLeft: widthPercentageToDP(6),
    marginTop: heightPercentageToDP(2),
  },
  textAddToCart: {
    color: 'white',
  },
  buttonAddToCart: {
    backgroundColor: 'mediumblue',
    width: widthPercentageToDP(30),
  },
  footer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginVertical: heightPercentageToDP(1),
  },
  noReview: {
    marginTop: heightPercentageToDP(1.5),
    color: 'grey',
    fontSize: 16,
  },
  image: {
    marginTop: heightPercentageToDP(5),
    alignItems: 'center',
    marginBottom: heightPercentageToDP(5),
  },
  textAllReview: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  textReadMore: {
    color: 'mediumblue',
    fontWeight: '500',
  },
  textSpesification: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  line: {
    borderBottomColor: 'black',
    borderBottomWidth: 1,
    marginVertical: heightPercentageToDP(1),
  },
  minOrder: {
    marginLeft: widthPercentageToDP(6),
    flexDirection: 'row',
    marginTop: heightPercentageToDP(0.5),
  },
  condition: {
    marginLeft: widthPercentageToDP(6),
    flexDirection: 'row',
    marginTop: heightPercentageToDP(1),
  },
  description: {
    marginLeft: widthPercentageToDP(6),
    marginTop: heightPercentageToDP(2),
  },
  paddingleft1: {
    paddingLeft: widthPercentageToDP(1),
  },
  flexrow: {
    flexDirection: 'row',
  },
  Avatar: {
    marginTop: 20,
    marginLeft: widthPercentageToDP(6),
    flexDirection: 'row',
  },
  textCompare: {
    color: 'white',
    fontWeight: 'bold',
    alignSelf: 'center',
    fontSize: 14,
  },
  compare: {
    backgroundColor: 'mediumblue',
    paddingVertical: 10,
    width: widthPercentageToDP(20),
    borderRadius: 5,
    marginLeft: widthPercentageToDP(6),
    marginTop: 15,
  },
  paddingleft10: {
    paddingLeft: 10,
  },
  title: {
    paddingHorizontal: widthPercentageToDP(6),
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  price: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: widthPercentageToDP(6),
  },
  swiper: {
    height: heightPercentageToDP(45),
    marginVertical: 10,
  },
  ScrollView: {
    flex: 1,
    paddingTop: 10,
    backgroundColor: '#DDDDDD',
  },
  appbar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    // borderBottomColor: 'grey',
    // borderBottomWidth: 1,
    paddingBottom: 10,
    marginTop: 10,
    alignItems: 'center',
  },
  SafeAreaView: {
    flex: 1,
  },
});
