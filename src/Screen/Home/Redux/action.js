export const GET_DATA_ALL = 'GET_DATA_ALL';
export const GET_DATA_ALL_SUCCESS = 'GET_DATA_ALL_SUCCESS';
export const GET_DATA_ALL_FAILED = 'GET_DATA_ALL_FAILED';
export const POST_DATA_ALL = 'POST_DATA_ALL';
export const GET_MORE = 'GET_MORE';
export const GET_MORE_SUCCESS = 'GET_MORE_SUCCESS';
export const GET_MORE_FAILED = 'GET_MORE_FAILED';
export const POST_MORE_DATA = 'POST_MORE_DATA';

export const getOnTrendingAction = payload => {
  return {type: GET_DATA_ALL, payload};
};
export const getOnTrendingActionSuccess = payload => {
  return {type: GET_DATA_ALL_SUCCESS, payload};
};
export const getOnTrendingActionFailed = payload => {
  return {type: GET_DATA_ALL_FAILED, payload};
};
export const getMoreAction = payload => {
  return {type: GET_MORE, payload};
};
export const getMoreSuccess = payload => {
  return {type: GET_MORE_SUCCESS, payload};
};
export const getMoreFailed = payload => {
  return {type: GET_MORE_FAILED, payload};
};
export const postOnTrendingAction = payload => {
  return {type: POST_DATA_ALL, payload};
};
export const postMoreAction = payload => {
  return {type: POST_MORE_DATA, payload};
};
