import {
  GET_DATA_ALL,
  GET_DATA_ALL_FAILED,
  GET_DATA_ALL_SUCCESS,
  POST_DATA_ALL,
  POST_MORE_DATA,
  GET_MORE,
  GET_MORE_FAILED,
  GET_MORE_SUCCESS,
} from './action';

const initialState = {
  data: [],
  newData: [],
  headers: [],
  page: 0,
  isLoading: false,
};

const homeReducer = (state = initialState, {type, payload}) => {
  switch (type) {
    case GET_DATA_ALL: {
      return {
        ...state,
        data: payload,
        isLoading: false,
      };
    }
    case GET_DATA_ALL_SUCCESS: {
      return {
        ...state,
        data: payload,
      };
    }
    case GET_DATA_ALL_FAILED: {
      return {
        ...state,
      };
    }
    case GET_MORE: {
      return {
        ...state,
        newData: payload,
        isLoading: false,
      };
    }
    case GET_MORE_SUCCESS: {
      const dataNew = payload;
      const {listData} = state;
      return {
        ...state,
        listData: [...listData, ...dataNew],
        isLoading: false,
      };
    }
    case GET_MORE_FAILED: {
      return {
        ...state,
      };
    }
    case POST_DATA_ALL: {
      return {
        ...state,
        data: payload,
        isLoading: true,
      };
    }
    case POST_MORE_DATA: {
      return {
        ...state,
        isLoading: false,
        headers: payload,
      };
    }
    default:
      return state;
  }
};

export default homeReducer;
