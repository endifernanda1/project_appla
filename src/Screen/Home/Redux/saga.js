import axios from 'axios';
import {takeLatest, put} from 'redux-saga/effects';
import {getOnTrendingAction, getMoreSuccess} from './action';
import {POST_DATA_ALL, POST_MORE_DATA} from './action';

function* getAllData(action) {
  try {
    const getAllDataApi = payload => {
      return axios({
        method: 'POST',
        url: 'https://appla-web.devs.id/api/product',
        data: payload,
      });
    };
    const result = yield getAllDataApi(action.payload);
    if (result.status === 200) {
      yield put(getOnTrendingAction(result.data.data));
    }
  } catch (error) {
    console.log(error.response, '<<<<< error getAllData');
  }
}

function* getMoreData(action) {
  try {
    const getMoreDataApi = payload => {
      return axios({
        method: 'POST',
        url: 'https://appla-web.devs.id/api/product',
        data: payload,
      });
    };
    const result = yield getMoreDataApi(action.payload);
    if (result.status === 200) {
      yield put(getMoreSuccess(result.data.data));
    }
  } catch (error) {
    console.log(error.response, '<<<<< error, getMoreData');
  }
}

export function* SagaGetData() {
  yield takeLatest(POST_DATA_ALL, getAllData);
  yield takeLatest(POST_MORE_DATA, getMoreData);
}
