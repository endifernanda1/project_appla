import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  Image,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  FlatList,
  LogBox,
  RefreshControl,
  ActivityIndicator,
} from "react-native";
import { Header, SearchBar } from "react-native-elements";

import FontAwesome from "react-native-vector-icons/FontAwesome";
import Foundation from "react-native-vector-icons/Foundation";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import Ionicons from "react-native-vector-icons/Ionicons";
import { styles } from "./styles";

import FastImage from "react-native-fast-image";
import logo from "../../Assets/Img/appla-logo.png";
import bg from "../../Assets/Img/hero-section.png";
import LinearGradient from "react-native-linear-gradient";

import { useDispatch, useSelector } from "react-redux";
import { postOnTrendingAction, postMoreAction } from "./Redux/action";
import ShimmerHome from "../../Assets/Component/ShimmerHome";
import axios from "axios";
import { useRef } from "react";

let stopFetch = true;

export default function Home(props) {
  const [search, updateSearch] = useState();
  const [refresh, setRefresh] = useState(false);
  // const [footer, setFooter] = useState(true);
  let [offset, setOffset] = useState(0);
  // let offset = 0;
  const [data, setData] = useState([]);
  const [dataRaw, setDataRaw] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [Loading, setLoading] = useState(true);
  // const dispatch = useDispatch();

  // const isLoading = useSelector(state => {
  //   return state.Home.isLoading;
  // });

  const onRefresh = () => {
    setRefresh(true);
    // setOffset(0);
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
    }, 3500);
    setRefresh(false);
  };

  useEffect(() => {
    getData(offset);
    setTimeout(() => {
      setLoading(false);
    }, 3500);
    LogBox.ignoreLogs(["VirtualizedLists should never be nested"]);
  }, []);

  // const dataSaga = useSelector(state => {
  //   return state.Home.data;
  // });

  // const newData = useSelector(state => {
  //   return state.Home.newData;
  // });

  const getData = async (initialData) => {
    try {
      if (initialData <= 70) {
        let headers = {
          token: "a3c3L1RFY3VLa3Q0R1JWVW1Scm9mZz09",
          action: "get_product",
          offset: initialData,
        };
        setIsLoading(true);

        let res = await axios.post(
          "https://appla-web.devs.id/api/product",
          headers
        );
        // axios.post("https://appla-web.devs.id/api/product", headers).then((res) => {
        //   //setUsers(res.data.results);
        //   // setDataRaw(res);
        //   setData([...data, ...res.data.data]);
        //   console.log(data, "data dari api");
        //   // console.log('🚀 ~ file: Home.js ~ line 135 ~ Home ~ headers', headers);
        //   // console.log("🚀 ~ file: Home.js ~ line 40 ~ Home ~ axios", dataRaw);
        //   setIsLoading(false);
        // });
        if (res.status === 200) {
          console.log(res, "result");
          await setData([...data, ...res.data.data]);
          await setOffset(initialData + 10);
          console.log(data, "data state");
          stopFetch = true;
          setIsLoading(false);
        } else {
          console.error(res);
        }
      } else {
        return;
      }
    } catch (error) {
      console.error(error);
    }
  };

  const renderLoader = () => {
    return isLoading ? (
      <View style={styles.loaderStyle}>
        {offset <= 70 ? (
          <ActivityIndicator size="large" color="#aaa" />
        ) : (
          <View />
        )}
      </View>
    ) : null;
  };

  const loadMoreItem = async () => {
    if (offset <= 70) {
      await setOffset(offset + 10);
      await getData(offset);
    }
  };

  const gotoDetail = (item) => {
    props.navigation.navigate(props.details, { data: item });
  };

  const onEndReachedCalledDuringMomentum = useRef(true);

  return (
    <SafeAreaView style={styles.safeAreaView}>
      <View style={styles.flex1}>
        <Header
          placement="left"
          backgroundColor="#a2ffe3"
          leftComponent={{
            icon: "menu",
            color: "#fff",
            iconStyle: { color: "#fff" },
          }}
          centerComponent={
            <Image source={logo} style={styles.logo} resizeMode={"contain"} />
          }
          rightComponent={
            <FontAwesome name="shopping-cart" color="#fff" size={24} />
          }
        />
        <View>
          <SearchBar
            placeholder="What are you looking for..."
            platform="ios"
            onChangeText={updateSearch}
            value={search}
            containerStyle={styles.searchbar}
          />
        </View>
        <ScrollView
          refreshControl={
            <RefreshControl refreshing={refresh} onRefresh={onRefresh} />
          }
        >
          <View style={styles.bg}>
            <LinearGradient
              colors={["#a2ffe3", "#a2ffae", "#efefef"]}
              style={styles.linearGradient}
            >
              <FastImage source={bg} style={styles.bgImages} />
            </LinearGradient>
          </View>
          <View style={styles.star}>
            <Foundation name="star" color="blue" size={16} />
            <Text style={styles.space}>On Trending</Text>
          </View>
          {Loading ? (
            <ShimmerHome />
          ) : (
            <View>
              <TouchableOpacity>
                <FlatList
                  data={data}
                  renderItem={({ item, index }) => (
                    <>
                      <SafeAreaView>
                        <View>
                          <TouchableOpacity style={styles.card}>
                            <View style={styles.imagesCard}>
                              <FastImage
                                source={{ uri: item.picture_thumbnail }}
                                style={styles.catalog}
                                resizeMode="stretch"
                              />
                            </View>
                            <Text numberOfLines={1}>{item.name1}</Text>
                            <Text style={styles.price}>{item.price}</Text>
                            <View style={styles.flexrow}>
                              <FontAwesome
                                name="building"
                                color="blue"
                                size={16}
                              />
                              <Text style={styles.grey} numberOfLines={1}>
                                {item.state}
                              </Text>
                            </View>
                            <View style={styles.flexrow}>
                              <MaterialCommunityIcons
                                name="star-circle"
                                color="grey"
                                size={16}
                              />
                              <Text style={styles.grey} numberOfLines={1}>
                                {item.store_name}
                              </Text>
                            </View>
                            <View style={styles.flexrow}>
                              <Foundation name="star" color="grey" size={16} />
                              <Text style={styles.grey}>
                                {item.store_rating} | {item.sold} sold
                              </Text>
                            </View>
                          </TouchableOpacity>
                        </View>
                      </SafeAreaView>
                    </>
                  )}
                  keyExtractor={(item) => item.product_id}
                  horizontal={true}
                  showsHorizontalScrollIndicator={true}
                  maxToRenderPerBatch={6}
                  initialNumToRender={4}
                  onEndReached={({ distanceFromEnd }) => {
                    if (!stopFetch) {
                      getData(offset);
                    }
                    console.log(distanceFromEnd);
                  }}
                  onScrollBeginDrag={() => (stopFetch = false)}
                  onEndReachedThreshold={0.01}
                  progressViewOffset={0}
                  windowSize={3}
                />
              </TouchableOpacity>
              <View style={styles.star}>
                <Ionicons name="pricetags" color="blue" size={16} />
                <Text style={styles.space}>All Product</Text>
              </View>
              <View style={styles.star} />
              <View style={styles.columns}>
                <FlatList
                  data={data}
                  renderItem={({ item, index }) => (
                    <>
                      <SafeAreaView>
                        <View>
                          <TouchableOpacity
                            style={styles.card}
                            // onPress={() => gotoDetail(item)}
                          >
                            <View style={styles.imagesCard}>
                              <FastImage
                                source={{ uri: item.picture_thumbnail }}
                                style={styles.catalog}
                                resizeMode="stretch"
                              />
                            </View>
                            <Text numberOfLines={1}>{item.name1}</Text>
                            <Text style={styles.price}>{item.price}</Text>
                            <View style={styles.flexrow}>
                              <FontAwesome
                                name="building"
                                color="blue"
                                size={16}
                              />
                              <Text style={styles.grey}>{item.state}</Text>
                            </View>
                            <View style={styles.flexrow}>
                              <MaterialCommunityIcons
                                name="star-circle"
                                color="grey"
                                size={16}
                              />
                              <Text style={styles.grey}>{item.store_name}</Text>
                            </View>
                            <View style={styles.flexrow}>
                              <Foundation name="star" color="grey" size={16} />
                              <Text style={styles.grey}>
                                {item.store_rating} | {item.sold} sold
                              </Text>
                            </View>
                          </TouchableOpacity>
                        </View>
                      </SafeAreaView>
                    </>
                  )}
                  keyExtractor={(item) => item.product_id}
                  numColumns={2}
                  key={"h"}
                  showsVerticalScrollIndicator={true}
                  onScrollAnimationEnd={true}
                  maxToRenderPerBatch={2}
                  initialNumToRender={2}
                  onEndReached={({ distanceFromEnd }) => {
                    if (!stopFetch) {
                      getData(offset);
                    }
                    console.log(distanceFromEnd);
                  }}
                  onScrollBeginDrag={() => (stopFetch = false)}
                  onEndReachedThreshold={1}
                  // onMomentumScrollBegin={() => {
                  //   onEndReachedCalledDuringMomentum.current = false;
                  // }}
                  ListFooterComponent={renderLoader}
                  progressViewOffset={2}
                  windowSize={2}
                  bounces={false}
                />
              </View>
            </View>
          )}
        </ScrollView>
      </View>
    </SafeAreaView>
  );
}

// let data = [...dataSaga, ...newData];

// const loadMoreV2 = () => {
//   const ITEMS_PER_PAGE = 13;
//   let page = 0;
//   let data = [];
//   const start = page * ITEMS_PER_PAGE;
//   const end = (page + 1) * ITEMS_PER_PAGE - 1;

//   const newData = dataSaga.slice(start, end);

//   setData([...data, ...newData]);
//   // Data.push([...data, ...newData]);
// };

// const loadData = data => {
//   let newData = data;

//   let interval = 2;
//   if (newData.length === 2) {
//     // setTimeout(() => {
//     return newData.slice(0, interval);
//     // }, 20000);
//   } else {
//     // setTimeout(() => {
//     let start = interval;
//     let end = interval * 2;
//     interval *= 2;
//     let existingData = [...newData];
//     let nextData = existingData.slice(start, end);
//     return nextData;
//     // }, 20000);
//   }
// };

// const loadMore = () => {
//   for (let index = 0; dataSaga.length && newData.length > 0; index++) {
//     headers.offset += 10;
//     dispatch(postMoreAction(headers));
//     let data = [...dataSaga, ...newData];
//     // dataSaga.push(...newData);
//     return data;
//   }
// };

// const add = () => {
//   if (newData.length > 0) {
//     headers.offset += 10;
//     // console.log(data)
//     dispatch(postMoreAction(headers));
//     setData([...dataSaga, ...newData]);
//     console.log(data);
//     return data;
//   }
// };

// const loadFirst = () => {
//   setOffset(0);
//   dispatch(postOnTrendingAction(headers));
// };

// const tes = console.log('test');

// const onEndReachedHandler = ({distanceFromEnd}) => {
//   if (!onEndReachedCalledDuringMomentum.current) {
//     add();
//     onEndReachedCalledDuringMomentum.current = true;
//   }
// };

// const Card = ({
//   title,
//   picture,
//   price,
//   state,
//   store_name,
//   store_rating,
//   sold,
// }) => (
//   <SafeAreaView>
//     <View>
//       <TouchableOpacity style={styles.card}>
//         <View style={styles.imagesCard}>
//           <FastImage
//             source={{uri: picture}}
//             style={styles.catalog}
//             resizeMode="stretch"
//           />
//         </View>
//         <Text numberOfLines={2}>{title}</Text>
//         <Text style={styles.price}>{price}</Text>
//         <View style={styles.flexrow}>
//           <FontAwesome name="building" color="blue" size={16} />
//           <Text style={styles.grey}>{state}</Text>
//         </View>
//         <View style={styles.flexrow}>
//           <MaterialCommunityIcons name="star-circle" color="grey" size={16} />
//           <Text style={styles.grey}>{store_name}</Text>
//         </View>
//         <View style={styles.flexrow}>
//           <Foundation name="star" color="grey" size={16} />
//           <Text style={styles.grey}>
//             {store_rating} | {sold} sold
//           </Text>
//         </View>
//       </TouchableOpacity>
//     </View>
//   </SafeAreaView>
// );

// const renderItem = ({item}) => (
//   <Card
//     title={item.name1}
//     picture={item.picture_thumbnail}
//     price={item.price}
//     state={item.state}
//     store_name={item.store_name}
//     store_rating={item.store_rating}
//     sold={item.sold}
//   />
// );
