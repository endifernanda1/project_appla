import {StyleSheet} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

export const styles = StyleSheet.create({
  flex1: {
    flex: 1,
    // paddingHorizontal: widthPercentageToDP(6),
  },
  columns: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    marginBottom: 20,
  },
  marginLeft: {
    marginLeft: 30,
  },
  shimmer: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  searchbar: {
    backgroundColor: '#a2ffe3',
  },
  linearGradient: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    width: widthPercentageToDP(100),
  },
  grey: {
    color: 'grey',
    paddingLeft: 8,
    alignItems: 'center',
  },
  flexrow: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 5,
  },
  imagesCard: {
    flex: 1,
    borderRadius: 10,
    marginBottom: 5,
  },
  card: {
    width: widthPercentageToDP(47),
    backgroundColor: 'white',
    padding: 15,
    borderRadius: 10,
    marginLeft: widthPercentageToDP(1.5),
    marginBottom: widthPercentageToDP(2),
    marginTop: 1,
  },
  catalog: {
    height: heightPercentageToDP(24),
    borderRadius: 15,
    marginBottom: 5,
  },
  price: {
    color: 'blue',
  },
  space: {
    paddingLeft: 5,
    alignItems: 'center',
  },
  star: {
    paddingLeft: 15,
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 5,
  },
  bg: {
    alignItems: 'center',
  },
  bgImages: {
    height: 150,
    width: 150,
  },
  logo: {
    height: 24,
    marginLeft: -80,
  },
  appbar: {
    flex: 1,
    backgroundColor: 'red',
  },
  safeAreaView: {
    flexGrow: 1,
    // paddingHorizontal: 10,
    backgroundColor: '#efefef',
  },
});
