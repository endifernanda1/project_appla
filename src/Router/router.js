import React from 'react';
// Navigation
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

// Screen
import Home from '../Screen/Home/home';
import HomeDetails from '../Screen/HomeDetails/homeDetails';

// Component

const Stack = createStackNavigator();

const Router = () => {
  //   useEffect(() => {
  //     setTimeout(() => {
  //       SplashScreen.hide();
  //     }, 1000);
  //   }, []);
  //   const isLogin = useSelector(state => state.LoginReducer.isLogin);

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={'Home'}>
        <Stack.Screen
          name="Home"
          component={Home}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name="HomeDetails"
          component={HomeDetails}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Router;
