import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {createShimmerPlaceholder} from 'react-native-shimmer-placeholder';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {
  widthPercentageToDP,
  heightPercentageToDP,
} from 'react-native-responsive-screen';

const ShimmerPlaceHolder = createShimmerPlaceholder(LinearGradient);

const ShimmerHome = () => {
  return (
    <View>
      <View style={styles.shimmer}>
        <View>
          <ShimmerPlaceHolder shimmerStyle={styles.catalog} />
          <ShimmerPlaceHolder shimmerStyle={styles.flexrow} />
          <ShimmerPlaceHolder shimmerStyle={styles.flexrow} />
          <ShimmerPlaceHolder shimmerStyle={styles.flexrow} />
        </View>
        <View style={styles.marginLeft}>
          <ShimmerPlaceHolder shimmerStyle={styles.catalog} />
          <ShimmerPlaceHolder shimmerStyle={styles.flexrow} />
          <ShimmerPlaceHolder shimmerStyle={styles.flexrow} />
          <ShimmerPlaceHolder shimmerStyle={styles.flexrow} />
        </View>
      </View>
      <View style={styles.star}>
        <Ionicons name="pricetags" color="blue" size={16} />
        <Text style={styles.space}>All Product</Text>
      </View>
      <View style={styles.shimmer}>
        <View>
          <ShimmerPlaceHolder shimmerStyle={styles.catalog} />
          <ShimmerPlaceHolder shimmerStyle={styles.flexrow} />
          <ShimmerPlaceHolder shimmerStyle={styles.flexrow} />
          <ShimmerPlaceHolder shimmerStyle={styles.flexrow} />
        </View>
        <View style={styles.marginLeft}>
          <ShimmerPlaceHolder shimmerStyle={styles.catalog} />
          <ShimmerPlaceHolder shimmerStyle={styles.flexrow} />
          <ShimmerPlaceHolder shimmerStyle={styles.flexrow} />
          <ShimmerPlaceHolder shimmerStyle={styles.flexrow} />
        </View>
      </View>
    </View>
  );
};

export default ShimmerHome;

const styles = StyleSheet.create({
  marginLeft: {
    marginLeft: 30,
  },
  flexrow: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: 5,
  },
  shimmer: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  catalog: {
    height: heightPercentageToDP(24),
    borderRadius: 15,
    marginBottom: 5,
  },
  price: {
    color: 'blue',
  },
  space: {
    paddingLeft: 5,
    alignItems: 'center',
  },
  star: {
    paddingLeft: 15,
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 5,
  },
});
