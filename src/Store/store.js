import {rootReducer} from './rootReducer';
import rootSaga from './rootSaga';
import {createStore, applyMiddleware} from 'redux';
import createSaga from 'redux-saga';
// import {persistStore, persistReducer} from 'redux-persist';
// import Storage from '@react-native-async-storage/async-storage';

import Logger from 'redux-logger';
//reducer

// const persistConfig = {
//   key: 'movieReview',
//   storage: Storage,
// };

const Saga = createSaga();
// const persistedReducer = persistReducer(persistConfig, AllReducer);

export const Store = createStore(rootReducer, applyMiddleware(Saga, Logger));

// const persistedStore = persistStore(Store);

export default Store;

Saga.run(rootSaga);
