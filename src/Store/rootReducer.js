import {combineReducers} from 'redux';
import globalReducer from './globalReducer';
import homeReducer from '../Screen/Home/Redux/reducer';

export const rootReducer = combineReducers({
  Global: globalReducer,
  Home: homeReducer,
});
