import {all} from 'redux-saga/effects';
import {SagaGetData} from '../Screen/Home/Redux/saga';

export default function* rootSaga() {
  yield all([SagaGetData()]);
}
